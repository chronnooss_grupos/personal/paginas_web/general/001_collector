<?php

    Class FuncionVideojuegos{

        static public function f_gameBuild($fields, $search, $platform, $genre, $theme, $limit, $token){

            $igdb = new IGDB("efbd78h8gkhku20i2assug435n3mpl", $token); 
            $builder = new IGDBQueryBuilder();

            // - Default
            // - Solo Plataforma
            // - Solo Genero
            // - Solo Temática
            // - Plataforma y genero
            // - Plataforma y temática
            // - Genero y temática
            // - Plataforma, genero y temática


            // Default
            if($platform == 0 && $genre == 0 && $theme == 0){
                $result = $igdb->game(
                                        $builder
                                            ->fields($fields)
                                            ->search($search)
                                            ->where("category != (5)")
                                            ->limit($limit)                                            
                                            ->build()
                                        );
            }
            // Solo plataforma
            if($platform > 0 && $genre == 0 && $theme == 0){
                $result = $igdb->game(
                                        $builder
                                            ->fields($fields)
                                            ->search($search)
                                            ->where("platforms = (".$platform.")")
                                            ->where("category != (5)")
                                            ->limit($limit)                                            
                                            ->build()
                                        );
            }
            // Solo genero
            if($platform == 0 && $genre > 0 && $theme == 0){
                $result = $igdb->game(
                                        $builder
                                            ->fields($fields)
                                            ->search($search)
                                            ->where("genres = (".$genre.")")
                                            ->where("category != (5)")
                                            ->limit($limit)                                            
                                            ->build()
                                        );
            }
            // Solo temática
            if($platform == 0 && $genre == 0 && $theme > 0){
                $result = $igdb->game(
                                        $builder
                                            ->fields($fields)
                                            ->search($search)
                                            ->where("themes = (".$theme.")")
                                            ->where("category != (5)")
                                            ->limit($limit)                                            
                                            ->build()
                                        );
            }
            // Plataforma y genero
            if($platform > 0 && $genre > 0 && $theme == 0){
                $result = $igdb->game(
                                        $builder
                                            ->fields($fields)
                                            ->search($search)
                                            ->where("platforms = (".$platform.")")
                                            ->where("genres = (".$genre.")")
                                            ->where("category != (5)")
                                            ->limit($limit)                                            
                                            ->build()
                                        );
            }
            // Plataforma y temática
            if($platform > 0 && $genre == 0 && $theme > 0){
                $result = $igdb->game(
                                        $builder
                                            ->fields($fields)
                                            ->search($search)
                                            ->where("platforms = (".$platform.")")
                                            ->where("themes = (".$theme.")")
                                            ->where("category != (5)")
                                            ->limit($limit)                                            
                                            ->build()
                                        );
            }
            // Genero y temática
            if($platform == 0 && $genre > 0 && $theme > 0){
                $result = $igdb->game(
                                        $builder
                                            ->fields($fields)
                                            ->search($search)
                                            ->where("genres = (".$genre.")")
                                            ->where("themes = (".$theme.")")
                                            ->where("category != (5)")
                                            ->limit($limit)                                            
                                            ->build()
                                        );
            }
            // Plataforma, genero y temática
            if($platform > 0 && $genre > 0 && $theme > 0){
                $result = $igdb->game(
                                        $builder
                                            ->fields($fields)
                                            ->search($search)
                                            ->where("platforms = (".$platform.")")
                                            ->where("genres = (".$genre.")")
                                            ->where("themes = (".$theme.")")
                                            ->where("category != (5)")
                                            ->limit($limit)                                            
                                            ->build()
                                        );
            }

            return $result;

        }

        static public function f_equivCategorias($categ){

            $result = "";

            switch ($categ) {
                case "0":
                    $result  =  "Juego principal";
                    break;
                case "1":
                    $result  =  "Dlc";
                    break;
                case "2":
                    $result  =  "Expansión";
                    break;
                case "3":
                    $result  =  "Bundle";
                    break;
                case "4":
                    $result  =  "Expansión independiente";
                    break;
                case "5":
                    $result  =  "Mod";
                    break;
                case "6":
                    $result  =  "Episodio";
                    break;
                case "7":
                    $result  =  "Temporada";
                    break;
                case "8":
                    $result  =  "Remake";
                    break;
                case "9":
                    $result  =  "Remaster";
                    break;
                case "10":
                    $result  =  "Juego expandido";
                    break;
                case "11":
                    $result  =  "Port";
                    break;
                case "12":
                    $result  =  "Fork";
                    break;
            }

            return $result;
        }

        static public function f_equivPlataformas($idPlatform, $namePlatform, $abrevPlatform){
            $result = "";

            switch ($idPlatform) {
                // Nintendo - Sobremesa
                case "18": $result = array("idPlatform" => $idPlatform, "namePlatform" => "Nintendo", "abbrevNamePlatform" => "NES", "classPlatform" => "bg-platform_nes");
                    break;
                case "19": $result = array("idPlatform" => $idPlatform, "namePlatform" => "Super Nintendo", "abbrevNamePlatform" => "SNES", "classPlatform" => "bg-platform_snes");
                    break;
                case "4": $result = array("idPlatform" => $idPlatform, "namePlatform" => "Nintendo 64", "abbrevNamePlatform" => "N64", "classPlatform" => "bg-platform_n64");
                    break;
                case "21": $result = array("idPlatform" => $idPlatform, "namePlatform" => "Nintendo GameCube", "abbrevNamePlatform" => "NGC", "classPlatform" => "bg-platform_ngc");
                    break;
                case "5": $result = array("idPlatform" => $idPlatform, "namePlatform" => "Nintendo Wii", "abbrevNamePlatform" => "Wii", "classPlatform" => "bg-platform_wii");
                    break;
                case "41": $result = array("idPlatform" => $idPlatform, "namePlatform" => "Nintendo Wii U", "abbrevNamePlatform" => "WiiU", "classPlatform" => "bg-platform_wiiu");
                    break;
                case "130": $result = array("idPlatform" => $idPlatform, "namePlatform" => "Nintendo Switch", "abbrevNamePlatform" => "Switch", "classPlatform" => "bg-platform_switch");
                    break;

                // Nintendo - Portatil
                case "33": $result = array("idPlatform" => $idPlatform, "namePlatform" => "Game Boy", "abbrevNamePlatform" => "GB", "classPlatform" => "bg-platform_gb");
                    break;
                case "22": $result = array("idPlatform" => $idPlatform, "namePlatform" => "Game Boy Color", "abbrevNamePlatform" => "GBC", "classPlatform" => "bg-platform_gbc");
                    break;
                case "24": $result = array("idPlatform" => $idPlatform, "namePlatform" => "Game Boy Advance", "abbrevNamePlatform" => "GBA", "classPlatform" => "bg-platform_gba");
                    break;
                case "20": $result = array("idPlatform" => $idPlatform, "namePlatform" => "Nintendo DS", "abbrevNamePlatform" => "NDS", "classPlatform" => "bg-platform_nds");
                    break;
                case "159": $result = array("idPlatform" => $idPlatform, "namePlatform" => "Nintendo DSi", "abbrevNamePlatform" => "DSi", "classPlatform" => "bg-platform_ndsi");
                    break;
                case "37": $result = array("idPlatform" => $idPlatform, "namePlatform" => "Nintendo 3DS", "abbrevNamePlatform" => "3DS", "classPlatform" => "bg-platform_3ds");
                    break;
                case "137": $result = array("idPlatform" => $idPlatform, "namePlatform" => "Nintendo New 3DS", "abbrevNamePlatform" => "n3DS", "classPlatform" => "bg-platform_n3ds");
                    break;

                // Sony - Sobremesa
                case "7": $result = array("idPlatform" => $idPlatform, "namePlatform" => "PlayStation", "abbrevNamePlatform" => "PS1", "classPlatform" => "bg-platform_ps1");
                    break;
                case "8": $result = array("idPlatform" => $idPlatform, "namePlatform" => "PlayStation 2", "abbrevNamePlatform" => "PS2", "classPlatform" => "bg-platform_ps2");
                    break;
                case "9": $result = array("idPlatform" => $idPlatform, "namePlatform" => "PlayStation 3", "abbrevNamePlatform" => "PS3", "classPlatform" => "bg-platform_ps3");
                    break;
                case "48": $result = array("idPlatform" => $idPlatform, "namePlatform" => "PlayStation 4", "abbrevNamePlatform" => "PS4", "classPlatform" => "bg-platform_ps4");
                    break;
                case "167": $result = array("idPlatform" => $idPlatform, "namePlatform" => "PlayStation 5", "abbrevNamePlatform" => "PS5", "classPlatform" => "bg-platform_ps5");
                    break;

                // Sony - Portatil
                case "38": $result = array("idPlatform" => $idPlatform, "namePlatform" => "PlayStation Portable", "abbrevNamePlatform" => "PSP", "classPlatform" => "bg-platform_psp");
                    break;
                case "46": $result = array("idPlatform" => $idPlatform, "namePlatform" => "PlayStation Vita", "abbrevNamePlatform" => "VITA", "classPlatform" => "bg-platform_psvita");
                    break;

                // Microsoft - Sobremesa
                case "11": $result = array("idPlatform" => $idPlatform, "namePlatform" => "Xbox", "abbrevNamePlatform" => "XBOX", "classPlatform" => "bg-platform_xbox");
                    break;
                case "12": $result = array("idPlatform" => $idPlatform, "namePlatform" => "Xbox 360", "abbrevNamePlatform" => "X360", "classPlatform" => "bg-platform_x360");
                    break;
                case "49": $result = array("idPlatform" => $idPlatform, "namePlatform" => "Xbox One", "abbrevNamePlatform" => "XONE", "classPlatform" => "bg-platform_xone");
                    break;
                case "169": $result = array("idPlatform" => $idPlatform, "namePlatform" => "Xbox Series", "abbrevNamePlatform" => "XSeries", "classPlatform" => "bg-platform_xseries");
                    break;
                
                // Sobremesa
                case "6": $result = array("idPlatform" => $idPlatform, "namePlatform" => "Pc", "abbrevNamePlatform" => "PC", "classPlatform" => "bg-platform_pc");
                    break;
                case "3": $result = array("idPlatform" => $idPlatform, "namePlatform" => "Linux", "abbrevNamePlatform" => "LINUX", "classPlatform" => "bg-platform_linux");
                    break;
                case "14": $result = array("idPlatform" => $idPlatform, "namePlatform" => "Mac", "abbrevNamePlatform" => "MAC", "classPlatform" => "bg-platform_mac");
                    break;

                // Moviles
                case "34": $result = array("idPlatform" => $idPlatform, "namePlatform" => "Ios", "abbrevNamePlatform" => "IOS", "classPlatform" => "bg-platform_ios");
                    break;
                case "39": $result = array("idPlatform" => $idPlatform, "namePlatform" => "Android", "abbrevNamePlatform" => "Android", "classPlatform" => "bg-platform_android");
                    break;
                
                // Otros
                case "170": $result = array("idPlatform" => $idPlatform, "namePlatform" => "Google Stadia", "abbrevNamePlatform" => "STADIA", "classPlatform" => "bg-platform_stadia");
                    break;
                case "385": $result = array("idPlatform" => $idPlatform, "namePlatform" => "Oculus Rift", "abbrevNamePlatform" => "O-Rift", "classPlatform" => "bg-platform_o_rift");
                    break;
                case "384": $result = array("idPlatform" => $idPlatform, "namePlatform" => "Oculus Quest", "abbrevNamePlatform" => "O-Quest", "classPlatform" => "bg-platform_o_quest");
                    break;
                case "113": $result = array("idPlatform" => $idPlatform, "namePlatform" => "Online", "abbrevNamePlatform" => "Online", "classPlatform" => "bg-platform_online");
                    break;
                case "82": $result = array("idPlatform" => $idPlatform, "namePlatform" => "Browser", "abbrevNamePlatform" => "Browser", "classPlatform" => "bg-platform_browser");
                    break;
                case "47": $result = array("idPlatform" => $idPlatform, "namePlatform" => "Virtual console", "abbrevNamePlatform" => "VC", "classPlatform" => "bg-platform_vc");
                    break;

                default: $result = array("idPlatform" => $idPlatform, "namePlatform" => $namePlatform, "abbrevNamePlatform" => $abrevPlatform, "classPlatform" => "bg-platform_other");
            }

            return $result;
        }
    }