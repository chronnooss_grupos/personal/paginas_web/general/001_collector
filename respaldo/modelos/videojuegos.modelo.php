<?php

    require_once "plugins/igdb/class.igdb.php";
    require_once "funciones/videojuegos.funcion.php";

    Class ModeloVideojuegos{

        static public function mdlObtenerToken(){
            $clientId = "efbd78h8gkhku20i2assug435n3mpl";
            $secretId = "7b2qntjppanz0amiw8w4puu661bst9";

            $igdbUtil = new IGDBUtils();
            $result = $igdbUtil -> authenticate($clientId, $secretId);
            return $result;
        }

        static public function mdlBuscarVideojuego($fields, $search, $platform, $genre, $theme, $limit, $token){
            
            $igdb = new IGDB("efbd78h8gkhku20i2assug435n3mpl", $token); 
            $igdbUtil = new IGDBUtils();
            $builder = new IGDBQueryBuilder();

            try {
                
                $result = FuncionVideojuegos::f_gameBuild($fields, $search, $platform, $genre, $theme, $limit, $token);
                
                foreach ($result as $key => $value){
            
                    $gameId = "";
                    $gameName = "";
                    $gameUrlCoverImage = "";

                    // Id
                    $gameId = $value->id;
                    // Nombre del juego
                    $gameName = $value->name;
                    // Id de la imagen cover
                    if(isset($value->cover->image_id)){
                        $gameUrlCoverImage = IGDBUtils::image_url($value->cover->image_id, "cover_big");
                    }
                    
                    $arrayGame[] = array('gameId' => $gameId, 
                                         'gameName' => $gameName, 
                                         'gameUrlCoverImage' => $gameUrlCoverImage
                                        );
                }                    

                return $arrayGame;

            } catch (IGDBInvalidParameterException $e) {
                // invalid parameter passed to the builder
                echo $e->getMessage();
            } catch (IGDBEndpointException $e) {
                // failed query
                echo $e->getMessage();
            }
        }

        static public function mdlDetalleVideojuego($fields, $id, $token){

            $igdb = new IGDB("efbd78h8gkhku20i2assug435n3mpl", $token);     
            $igdbUtil = new IGDBUtils();
            $builder = new IGDBQueryBuilder();
            
            try {

                $result = $igdb->game(
                                        $builder
                                            ->fields($fields)
                                            ->where("id = ".$id)
                                            ->build()
                                        );
                
                                        

                foreach ($result as $key => $value){
                    // Variables
                    $game_id = ""; // Si
                    $game_name = ""; // Si 
                    $game_firstDateRelease = ""; // Si

                    $game_involvedCompanies = array(); // Si
                    $game_themes = array(); // Si
                    $game_genres = array(); // Si
                    $game_platforms = array(); // Si
                    $game_gameModes = array(); // Si
                    $game_webSites = array(); // Si
                    $game_coverImageUrl = ""; // Si
                    $game_screenshots = array();
                    $game_franchises = array();
                    $game_collections = array();
                    $game_similarGames = array(); 

                    $arrayGame = array();
                    // Id
                    if(isset($value->id)){
                        $game_id = $value->id;
                    }
                    // Name
                    if(isset($value->name)){
                        $game_name = $value->name;
                    }
                    // First Date Release
                    if(isset($value->first_release_date)){
                        $game_firstDateRelease = gmdate("Y", $value->first_release_date);
                    }
                    // Name of Involded Companies and Company
                    if(isset($value->involved_companies)){
                        foreach ($value->involved_companies as $key2 => $value2) {
                            // echo "name of company -----> ".$value2->company->name." <------- ";
                            $game_involvedCompanies[] = array('isDeveloper' => $value2->developer, 'isPublisher' => $value2->publisher, 'nameCompany' => $value2->company->name);
                        }
                    }
                    // Themes
                    if(isset($value->themes))
                    {
                        foreach ($value->themes as $key2 => $value2) {
                            if(isset($value2->name) && $value2->name != null){
                                $game_themes[] = array('nameTheme' => $value2->name);
                            }
                        }
                    }
                    // Genres
                    if(isset($value->genres))
                    {
                        foreach ($value->genres as $key2 => $value2) {
                            if(isset($value2->name) && $value2->name != null){
                                $game_genres[] = array('nameGenre' => $value2->name);
                            }
                        }
                    }
                    // Platforms
                    if(isset($value->platforms))
                    {
                        foreach ($value->platforms as $key2 => $value2) {
                            if(isset($value2->name)){
                                if(isset($value2->abbreviation)){
                                    $game_platforms[] = FuncionVideojuegos::f_equivPlataformas($value2->id, $value2->name, $value2->abbreviation);
                                }else{
                                    $game_platforms[] = FuncionVideojuegos::f_equivPlataformas($value2->id, $value2->name, $value2->name);
                                }
                            }
                        }
                    }
                    // GameModes
                    if(isset($value->game_modes))
                    {
                        foreach ($value->game_modes as $key2 => $value2) {
                            if(isset($value2->name) && $value2->name != null){
                                $game_gameModes[] = array('nameGameMode' => $value2->name);
                            }
                        }
                    }
                    // Websites
                    if(isset($value->websites))
                    {
                        foreach ($value->websites as $key2 => $value2) {
                            if(isset($value2->url) && $value2->url != null){
                                if($value2->category == 1)
                                {
                                    $game_webSites[] = array('nameWebsite' => 'Official','urlWebsite' => $value2->url);
                                }
                                
                                if($value2->category == 2)
                                {
                                    $game_webSites[] = array('nameWebsite' => 'Wikia','urlWebsite' => $value2->url);
                                }

                                if($value2->category == 3)
                                {
                                    $game_webSites[] = array('nameWebsite' => 'Wikipedia','urlWebsite' => $value2->url);
                                }
                            }
                        }
                    }
                    // Cover URL
                    if(isset($value->cover->image_id)){
                        $game_coverImageUrl = IGDBUtils::image_url($value->cover->image_id, "720p");
                    }else{
                        $game_coverImageUrl = "https://st3.depositphotos.com/23594922/31822/v/600/depositphotos_318221368-stock-illustration-missing-picture-page-for-website.jpg";
                    }
                     // Screenshots
                    if(isset($value->screenshots))
                    {
                        foreach ($value->screenshots as $key2 => $value2) {
                            if(isset($value2->image_id)){
                                $game_screenshotImageUrl[] = array('urlScreenshot' => IGDBUtils::image_url($value2->image_id, "cover_big"));
                            }else{
                                $game_screenshotImageUrl[] = array('urlScreenshot' => "https://st3.depositphotos.com/23594922/31822/v/600/depositphotos_318221368-stock-illustration-missing-picture-page-for-website.jpg");
                            }
                        }
                    }

                    // Name of Franchises and Franchise
                    if(isset($value->franchises)){
                        foreach ($value->franchises as $key2 => $value2) {
                            foreach ($value2->games as $key3 => $value3) {
                                if(isset($value3->cover->image_id)){
                                    $game_franchises[] = array('nameFranchise' => $value2->name, 'nameGame' => $value3 -> name, 'urlImage' => IGDBUtils::image_url($value3->cover->image_id, "cover_big"));
                                }else{
                                    $game_franchises[] = array('nameFranchise' => $value2->name, 'nameGame' => $value3 -> name, 'urlImage' => 'https://st3.depositphotos.com/23594922/31822/v/600/depositphotos_318221368-stock-illustration-missing-picture-page-for-website.jpg');
                                }
                            }
                        }
                    }
                    // Name of Collection and Collection
                    if(isset($value->collection)){
                        foreach ($value->collection->games as $key2 => $value2) {
                                if(isset($value2->cover->image_id)){
                                    $game_collections[] = array('nameSerie' => $value->collection->name, 'nameGame' => $value2 -> name, 'urlImage' => IGDBUtils::image_url($value2->cover->image_id, "cover_big"));
                                }else{
                                    $game_collections[] = array('nameSerie' => $value->collection->name, 'nameGame' => $value2 -> name, 'urlImage' => 'https://st3.depositphotos.com/23594922/31822/v/600/depositphotos_318221368-stock-illustration-missing-picture-page-for-website.jpg');
                                }
                            // }
                        }
                    }
                    // Name of Similar Games and Similar Games
                     if(isset($value->similar_games)){
                        foreach ($value->similar_games as $key2 => $value2) {
                            if(isset($value2->cover->image_id)){
                                $game_similarGames[] = array('nameFranchise' => $value2->name, 'nameGame' => $value2 -> name, 'urlImage' => IGDBUtils::image_url($value2->cover->image_id, "cover_big"));
                            }else{
                                $game_similarGames[] = array('nameFranchise' => $value2->name, 'nameGame' => $value2 -> name, 'urlImage' => 'https://st3.depositphotos.com/23594922/31822/v/600/depositphotos_318221368-stock-illustration-missing-picture-page-for-website.jpg');
                            }
                        }
                    }



                    $arrayGame[] = array('game_id' => $game_id, 
                                         'game_name' => $game_name, 
                                         'game_firstDateRelease' => $game_firstDateRelease, 
                                         'game_involvedCompanies' => $game_involvedCompanies, 
                                         'game_themes' => $game_themes, 
                                         'game_genres' => $game_genres, 
                                         'game_platforms' => $game_platforms, 
                                         'game_gameModes' => $game_gameModes, 
                                         'game_webSites' => $game_webSites,
                                         'game_coverImageUrl' => $game_coverImageUrl,
                                         'game_screenshotImageUrl' => $game_screenshotImageUrl,
                                         'game_franchises' => $game_franchises,
                                         'game_collections' => $game_collections,
                                         'game_similarGames' => $game_similarGames
                                        );
                }                 

                return $arrayGame;

            } catch (IGDBInvalidParameterException $e) {
                // invalid parameter passed to the builder
                echo $e->getMessage();
                return $e->getMessage();
            } catch (IGDBEndpointException $e) {
                // failed query
                echo $e->getMessage();
                return $e->getMessage();
            }
        }
    } 
?>