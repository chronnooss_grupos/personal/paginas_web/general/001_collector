<!--=============================================
=            Include Css Base           =
==============================================-->

<!-- Bootstrap: Base -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
    integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
<!-- Bootstrap: Datatable -->
<link rel="stylesheet" href="<?php echo SERVERURL; ?>vistas/assets/css/inc/plugins/dataTables.bootstrap5.min.css">

<!-- Icons: FontAwesome -->
<script src="https://kit.fontawesome.com/a21f4124e0.js" crossorigin="anonymous"></script>
<!-- Icons: Boxicons -->
<link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>

<!-- Plugin: Rater JS -->
<link rel="stylesheet" href="<?php echo SERVERURL; ?>vistas/assets/js/plugins/rater-js/style.css">
<!-- Plugin: Toastr JS -->
<link rel="stylesheet" href="<?php echo SERVERURL; ?>vistas/assets/css/inc/plugins/toastr.min.css">
<!-- Plugin: Owl Carousel -->
<link rel="stylesheet" href="<?php echo SERVERURL; ?>vistas/assets/css/inc/plugins/owl.carousel.min.css">

<!-- Custom Css -->
<link rel="stylesheet" href="<?php echo SERVERURL; ?>vistas/assets/css/base.css">
<link rel="stylesheet" href="<?php echo SERVERURL; ?>vistas/assets/css/juegos_badges.css">
<link rel="stylesheet" href="<?php echo SERVERURL; ?>vistas/assets/css/juegos_busqueda.css">
<link rel="stylesheet" href="<?php echo SERVERURL; ?>vistas/assets/css/juegos_listado.css">

