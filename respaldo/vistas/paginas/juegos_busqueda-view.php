
<div class="main__content busquedaVideojuegos">
    <div class="card seccion_formulario">
        <form id="formBusquedaJuegos">
            <h3>Game search</h3>

            <div class="entradas">
                <div>
                    <label for="busquedaJuego" class="form-label">Title</label>
                    <input type="text" class="form-control" id="txtBusquedaJuego" aria-label="Título">
                </div>

                <div>
                    <label for="selPlataformas" class="form-label">Platform</label>
                    <select class="form-select" id="selPlataformas">
                        <option value="0">All platforms</option>
                        <optgroup label="Nintendo">
                            <option disabled>Home Consoles</option>
                            <option value="18">Nintendo</option>
                            <option value="19">Super Nintendo</option>
                            <option value="4">Nintendo 64</option>
                            <option value="21">Nintendo GameCube</option>
                            <option value="5">Nintendo Wii</option>
                            <option value="41">Nintendo Wii U</option>
                            <option value="130">Nintendo Switch</option>
                            <option disabled>Portable Consoles</option>
                            <option value="33">Game Boy</option>
                            <option value="22">Game Boy Color</option>
                            <option value="24">Game Boy Advance</option>
                            <option value="20">Nintendo DS</option>
                            <option value="159">Nintendo DSi</option>
                            <option value="37">Nintendo 3DS</option>
                            <option value="137">Nintendo New 3ds</option>
                        </optgroup>
                        <optgroup label="Sony">
                            <option disabled>Home Consoles</option>
                            <option value="7">PlayStation 1</option>
                            <option value="8">PlayStation 2</option>
                            <option value="9">PlayStation 3</option>
                            <option value="48">PlayStation 4</option>
                            <option value="167">PlayStation 5</option>
                            <option disabled>Portable Consoles</option>
                            <option value="38">PlayStation Portatil</option>
                            <option value="46">PlayStation Vita</option>
                        </optgroup>
                        <optgroup label="Microsoft">
                            <option disabled>Home Consoles</option>
                            <option value="11">Xbox</option>
                            <option value="12">Xbox 360</option>
                            <option value="49">Xbox One</option>
                            <option value="169">Xbox Series</option>
                        </optgroup>
                        <optgroup label="PC">
                            <option value="6">Windows</option>
                            <option value="3">Linux</option>
                            <option value="14">Mac</option>
                        </optgroup>
                        <optgroup label="Mobile">
                            <option value="34">Ios</option>
                            <option value="39">Android</option>
                        </optgroup>
                        <optgroup label="Other Systems ">
                            <option value="170">Google Stadia</option>
                            <option value="385">Oculus-Rift</option>
                            <option value="384">Oculus-Quest</option>
                            <option value="113">Online</option>
                            <option value="82">Browser</option>
                            <option value="47">Virtual Console</option>
                        </optgroup>
                    </select>
                </div>

                <div>
                    <label for="selGeneros" class="form-label">Genre</label>
                    <select class="form-select" id="selGeneros">
                        <option value="0">All genres</option>
                            <option value="31">Adventure</option>
                            <option value="33">Arcade</option>
                            <option value="35">Card & Board Game</option>
                            <option value="4">Fighting</option>
                            <option value="25">Hack and slash/Beat 'em up</option>
                            <option value="32">Indie</option>
                            <option value="36">MOBA</option>
                            <option value="7">Music</option>
                            <option value="30">Pinball</option>
                            <option value="8">Platform</option>
                            <option value="2">Point-and-click</option>
                            <option value="9">Puzzle</option>
                            <option value="26">Quiz/Trivia</option>
                            <option value="10">Racing</option>
                            <option value="11">Real Time Strategy (RTS)</option>
                            <option value="12">Role-playing (RPG)</option>
                            <option value="5">Shooter</option>
                            <option value="13">Simulator</option>
                            <option value="14">Sport</option>
                            <option value="15">Strategy</option>
                            <option value="24">Tactical</option>
                            <option value="16">Turn-based strategy (TBS)</option>
                            <option value="34">Visual Novel</option>
                    </select>
                </div>

                <div>
                    <label for="selTemas" class="form-label">Theme</label>
                    <select class="form-select" id="selTemas">
                        <option value="0">All themes</option>
                            <option value="41">4X (explore, expand, exploit, and exterminate)</option>
                            <option value="1">Action</option>
                            <option value="28">Business</option>
                            <option value="27">Comedy</option>
                            <option value="31">Drama</option>
                            <option value="34">Educational</option>
                            <option value="42">Erotic</option>
                            <option value="17">Fantasy</option>
                            <option value="22">Historical</option>
                            <option value="19">Horror</option>
                            <option value="35">Kids</option>
                            <option value="43">Mystery</option>
                            <option value="32">Non-fiction</option>
                            <option value="38">Open world</option>
                            <option value="40">Party</option>
                            <option value="44">Romance</option>
                            <option value="33">Sandbox</option>
                            <option value="18">Science fiction</option>
                            <option value="23">Stealth</option>
                            <option value="21">Survival</option>
                            <option value="20">Thriller</option>
                            <option value="39">Warfare</option>
                    </select>
                </div>

                <div>
                    <label for="limiteBusquedaJuego" class="form-label">Limit</label>
                    <input type="number" id="selLimite" class="form-control" value="20" min="1" max="500" />

                </div>
            </div>

            <div class="text-center">
                <button type="submit" id="buttonBusquedaJuego"
                    class="btn btn-info boton_enviar form-control">Search</button>
            </div>

        </form>
    </div>

    <div id="checkBiblioteca" class="card seccion_biblioteca">
        <div class="card__titulo">
            <p>Library</p>
        </div>

        <div id="checkBiblioteca__galeria" class="galeria">
            
        </div>
    </div>
</div>

<!-- Modal -->

<div class="modal fade modalDetalleVideojuego" id="modalBusquedaVideojuegos" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl modal-dialog-scrollable">
    <div class="modal-content">
      <div class="modal-body">
        <div class="modal_info card">
            <div class="container">
                <div class="row">
                    <div class="col-4 modal_info--attribute">
                        <h5>Name</h5>
                    </div>
                    <div class="col modal_info--value" id="modal__info--name"></div>
                </div>
                <div class="row">
                    <div class="col-4 modal_info--attribute">
                        <h5>First Date Release</h5>
                    </div>
                    <div class="col modal_info--value" id="modal__info--firstDateRelease"></div>
                </div>
                <div class="row">
                        <div class="col-4 modal_info--attribute">
                            <h5>Developer</h5>
                        </div>
                        <div class="col modal_info--value" id="modal__info--developer"></div>
                </div>
                <div class="row">
                        <div class="col-4 modal_info--attribute">
                            <h5>Publisher</h5>
                        </div>
                        <div class="col modal_info--value" id="modal__info--publisher"></div>
                </div>
                <div class="row">
                        <div class="col-4 modal_info--attribute">
                            <h5>Themes</h5>
                        </div>
                        <div class="col modal_info--value" id="modal__info--themes"></div>
                </div>
                <div class="row">
                        <div class="col-4 modal_info--attribute">
                            <h5>Genres</h5>
                        </div>
                        <div class="col modal_info--value" id="modal__info--genres"></div>
                </div>
                <div class="row">
                        <div class="col-4 modal_info--attribute">
                            <h5>Platforms</h5>
                        </div>
                        <div class="col modal_info--value" id="modal__info--platforms"></div>
                </div>
                <div class="row">
                        <div class="col-4 modal_info--attribute">
                            <h5>Game Modes</h5>
                        </div>
                        <div class="col modal_info--value" id="modal__info--gameModes"></div>
                </div>
                <div class="row">
                        <div class="col-4 modal_info--attribute">
                            <h5>Websites</h5>
                        </div>
                        <div class="col modal_info--value" id="modal__info--webSites"></div>
                </div>
            </div>
            
        </div>
        <div class="modal_image card" id="modal__image--cover">
            <div>
                <img src="https://images.igdb.com/igdb/image/upload/t_720p/co217n.jpg" alt="">
            </div>
        </div>
        <div class="modal_screenshots card">
            <h5>Images</h5>
            <div class="feed-area-images owl-carousel" id="modal__screenshots--carousel">
            </div>
        </div>
        <div class="modal_franchises card">
            <h5>Franchises</h5>
            <div class="feed-area owl-carousel" id="modal_franchises--carousel">
            </div>
        </div>
        <div class="modal_series card">
            <h5>Series</h5>
            <div class="feed-area owl-carousel" id="modal_series--carousel">
            </div>
        </div>
        <div class="modal_similar card">
            <h5>Similar Games</h5>
            <div class="feed-area owl-carousel" id="modal_similar--carousel">
            </div>
        </div>
      </div>
      <div class="modal-footer" style="text-align: center;">
        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary">Guardar</button>
      </div>
    </div>
  </div>
</div>