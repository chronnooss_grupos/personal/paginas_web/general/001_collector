<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

	<title><?php echo COMPANY; ?></title>

    <!--=============================================
    =            Include Css files           =
    ==============================================-->
	<?php include "./vistas/inc_recursos/css_base.php"; ?>

</head>
<body>

    <?php
    
        require_once "./controladores/vistas.controlador.php";
        $IV = new vistasControlador();

        $vistas = $IV -> obtener_vistas_controlador();

        if($vistas == "login" || $vistas == "404"){
            require_once "./vistas/paginas/".$vistas."-view.php";
        }else{
                   
    ?>

    <!-- BEGIN: Sidebar-->
    <?php 
        include "./vistas/inc_plantilla/sidebar.php"; ?>
    <!-- END: Sidebar-->

    <section class="main">
        <div class="main__header">
            <i class='bx bx-menu'></i>
           <div class="themeCheck">
                <input type="checkbox" class="themeCheck__checkbox" id="chk" />
                <label class="themeCheck__label" for="chk">
                    <i class="fas fa-sun"></i>
                    <i class="fas fa-moon"></i>
                    <div class="ball"></div>
                </label>
            </div>
        </div>

        <?php  
            include $vistas;
        ?>
                
    </section>

    <?php

        }
    
    ?>

    <!--=============================================
    =            Include JavaScript files           =
    ==============================================-->
    <?php include "./vistas/inc_recursos/js_base.php"; ?>
</body>
</html>