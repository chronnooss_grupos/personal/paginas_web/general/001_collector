<div class="sidebar close">
    <div class="logo-details">
        <i class='bx bxl-c-plus-plus'></i>
        <span class="logo_name">Collector</span>
    </div>
    <ul class="nav-links">
        <li>
            <a href="inicio">
                <i class='fas fa-home'></i>
                <span class="link_name">Dashboard</span>
            </a>
            <ul class="sub-menu blank">
                <li><a class="link_name" href="inicio">Dashboard</a></li>
            </ul>
        </li>
        <li>
            <div class="icon-link">
                <a href="#">
                    <i class='fas fa-gamepad'></i>
                    <span class="link_name">Videogame</span>
                </a>
                <i class='bx bxs-chevron-down arrow'></i>
            </div>
            <ul class="sub-menu">
                <li><a class="link_name" href="#">Videogame</a></li>
                <li><a href="juegos_busqueda">Search</a></li>
                <li><a href="juegos_listado">My list</a></li>
                <!-- <li><a href="#">Favoritos</a></li>
                <li><a href="#">Tops</a></li>
                <li><a href="#">Deseados</a></li>
                <li><a href="#">Terminados</a></li>
                <li><a href="#">En Youtube</a></li> -->
            </ul>
        </li>
        <!-- <li>
            <div class="icon-link">
                <a href="#">
                    <i class='fas fa-tv'></i>
                    <span class="link_name">Tv</span>
                </a>
                <i class='bx bxs-chevron-down arrow'></i>
            </div>
            <ul class="sub-menu">
                <li><a class="link_name" href="#">Películas, series o anime</a></li>
                <li><a href="#">Búsqueda</a></li>
                <li><a href="#">Mi lista</a></li>
                <li><a href="#">Favoritos</a></li>
                <li><a href="#">Tops</a></li>
                <li><a href="#">Deseados</a></li>
                <li><a href="#">Terminados</a></li>
            </ul>
        </li>
        <li>
            <div class="icon-link">
                <a href="#">
                    <i class='fas fa-book'></i>
                    <span class="link_name">Libros</span>
                </a>
                <i class='bx bxs-chevron-down arrow'></i>
            </div>
            <ul class="sub-menu">
                <li><a class="link_name" href="#">Libros</a></li>
                <li><a href="#">Búsqueda</a></li>
                <li><a href="#">Mi lista</a></li>
                <li><a href="#">Favoritos</a></li>
                <li><a href="#">Tops</a></li>
                <li><a href="#">Deseados</a></li>
                <li><a href="#">Terminados</a></li>
            </ul>
        </li> -->
        <li>
            <a href="#">
                <i class='bx bx-cog'></i>
                <span class="link_name">Configuration</span>
            </a>
            <ul class="sub-menu blank">
                <li><a class="link_name" href="#">Configuration</a></li>
            </ul>
        </li>
        <li>
            <div class="profile-details">
                <div class="name-job">
                    <div class="profile_name">Andrés Echagüe</div>
                    <div class="job">Developer</div>
                </div>
                <i class='bx bx-log-out'></i>
            </div>
        </li>
    </ul>
</div>