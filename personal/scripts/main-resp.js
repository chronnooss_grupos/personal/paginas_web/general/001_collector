(async () => {
   ("use strict");

   // Obteniendo el token
   const obtenerToken = () => {
      const clientid = "efbd78h8gkhku20i2assug435n3mpl";
      const secretid = "0il9c4mz6r4i5lkd6hcpxq4cjmma9q";

      //   const url = "https://cors-anywhere.herokuapp.com/https://id.twitch.tv/oauth2/token?client_id=" + clientid + "&client_secret=" + secretid + "&grant_type=client_credentials";
      const url = "https://id.twitch.tv/oauth2/token?client_id=" + clientid + "&client_secret=" + secretid + "&grant_type=client_credentials";
      const requestOptionsApi = {
         method: "POST",
         mode: "cors",
      };

      const result = $.post(url, requestOptionsApi, (response) => {
         //   console.log(response);
         return response;
      });

      return result;
   };

   const token = await obtenerToken();
   //    console.log(token.access_token);

   //    const apiIGDB = () => {
   //       const url = "https://cors-anywhere.herokuapp.com/https://api.igdb.com/v4/games";
   //       //   const url = "https://api.igdb.com/v4/games";

   //       const requestOptionsApi = {
   //          method: "POST",
   //          //  mode: "cors",
   //          data: 'fields name; search "Halo"; limit 500;',
   //          headers: {
   //             Accept: "application/json",
   //             // "Access-Control-Allow-Origin": "*",
   //             "Client-ID": "efbd78h8gkhku20i2assug435n3mpl",
   //             Authorization: `Bearer ${token.access_token}`,
   //          },
   //       };

   //       const result = $.post(url, requestOptionsApi, (response) => {
   //          console.log(response);
   //          return response;
   //       });

   //       return result;
   //    };

   axios({
      url: "https://cors-anywhere.herokuapp.com/https://api.igdb.com/v4/games",
      method: "POST",
      headers: {
         Accept: "application/json",
         "Client-ID": "efbd78h8gkhku20i2assug435n3mpl",
         Authorization: `Bearer ${token.access_token}`,
      },
      data: 'fields name; search "Mario bros"; limit 500;',
   })
      .then((response) => {
         console.log(response.data);
      })
      .catch((err) => {
         console.error(err);
      });

   //    let token = await obtenerToken();
   //    console.log(token.access_token);

   //    let resultIgdb = await apiIGDB();
   //    console.log({ resultIgdb });
})();
