(() => {
   ("use strict");

   // Obteniendo el token
   const obtenerToken = async () => {
      const clientid = "efbd78h8gkhku20i2assug435n3mpl";
      const secretid = "0il9c4mz6r4i5lkd6hcpxq4cjmma9q";

      try {
         const respuesta = await axios(`https://id.twitch.tv/oauth2/token?client_id=${clientid}&client_secret=${secretid}&grant_type=client_credentials`, {
            method: "POST",
            mode: "cors",
         });

         // console.log(respuesta.data.access_token)
         return respuesta.data.access_token;
      } catch (error) {
         console.log(error);
      }
   };

   // Api: IGDB
   const getJuegos = async () => {
      const jsonToken = await obtenerToken();

      try {
         const respuesta = await axios("https://cors-anywhere.herokuapp.com/https://api.igdb.com/v4/games", {
            method: "POST",
            mode: "cors",
            headers: {
               Accept: "application/json",
               "Client-ID": "efbd78h8gkhku20i2assug435n3mpl",
               Authorization: `Bearer ${jsonToken}`,
            },
            data: `fields name, cover.image_id;
                  search "Mario bros";
                  limit 40;`,
         });

         // console.log(respuesta.data)
         // Si la respuesta es correcta
         if (respuesta.status === 200) {
            // const datos = await respuesta.json();
            respuesta.data.forEach((juego) => {
               const [nombre, image_url] = [juego.name, `https://images.igdb.com/igdb/image/upload/t_720p/${juego.cover.image_id}.png`];

               console.log({ nombre, image_url });
            });
         } else {
            console.log("Ha ocurrido un error");
         }
      } catch (error) {
         console.log(error);
      }
   };

   getJuegos();
})();
