(async() => {
   ("use strict");

   // Obteniendo el token
   const obtenerToken = () => {
      const clientid = "efbd78h8gkhku20i2assug435n3mpl";
      const secretid = "0il9c4mz6r4i5lkd6hcpxq4cjmma9q";

      const url = "https://id.twitch.tv/oauth2/token?client_id=" + clientid + "&client_secret=" + secretid + "&grant_type=client_credentials";
      const requestOptionsApi = {
         method: "POST",
         mode: "cors",
      };

      const result = $.post(url, requestOptionsApi, (response) => {
         return response;
      });

      return result;
   };

   const jsonToken = await obtenerToken();

})();
