(() => {
   ("use strict");

   // Elementos html
   const btnBusqueda = document.querySelector("#buttonBusquedaJuego");

   /*--=============================================
    =            Funciones / Métodos            =
    ===============================================*/
   // Función que construye el objeto literal a partir del formulario
   const f_constructDataApi = (search, platform, genre, theme, limit, jsonToken) => {
      let data = "";
      data = `fields id, name, cover.image_id;`;
      data += `search "${search}";`;

      data += `where category != (5)`;

      if (platform > 0) {
         data += ` & platforms = (${platform})`;
      }
      if (genre > 0) {
         data += ` & genres = (${genre})`;
      }
      if (theme > 0) {
         data += ` & themes = (${theme})`;
      }
      data += `;`;
      data += `limit ${limit};`;

      return data;
   };

   /*--=============================================
    =            Funciones asincronas         =
    ===============================================*/
   // Función para obtener el token de Twitch
   const getToken = async () => {
      const clientid = "efbd78h8gkhku20i2assug435n3mpl";
      const secretid = "dm7qndukt3knxrdmief0jhtjhkt1ea";

      try {
         const respuesta = await axios(`https://id.twitch.tv/oauth2/token?client_id=${clientid}&client_secret=${secretid}&grant_type=client_credentials`, {
            method: "POST",
            mode: "cors",
         });

         // console.log(respuesta.data.access_token)
         return respuesta.data.access_token;
      } catch (error) {
         console.log(error);
      }
   };

   // Función para obtener un listado de videojuegos
   const getGames = async () => {
      // Inicialización
      let jsonToken = await getToken();
      let checkGallery = document.getElementById("checkGallery");
      let galleryContainer = document.getElementById("checkGallery__container");

      // Formulario
      let search = $("#txtBusquedaJuego").val();
      let platform = $("#selPlataformas option:selected").val();
      let genre = $("#selGeneros option:selected").val();
      let theme = $("#selTemas option:selected").val();
      let limit = $("#selLimite").val();

      // Limpiar campos
      galleryContainer.innerHTML = "";
      let gamesGallery = "";
      let gG = "";
      let dataApi = f_constructDataApi(search, platform, genre, theme, limit, jsonToken);

      console.log({ dataApi });
      try {
         const respuesta = await axios("https://cors-anywhere.herokuapp.com/https://api.igdb.com/v4/games", {
            method: "POST",
            mode: "cors",
            headers: {
               Accept: "application/json",
               "Client-ID": "efbd78h8gkhku20i2assug435n3mpl",
               Authorization: `Bearer ${jsonToken}`,
            },
            data: `${dataApi}`,
         });

         // console.log(respuesta.data)
         galleryContainer.innerHTML = "";
         let gamesGallery = "";
         let gG = "";

         // Si la respuesta es correcta
         if (respuesta.status === 200) {
            // const datos = await respuesta.json();
            respuesta.data.forEach((juego) => {
               const [gameId, gameName, gameImage_id, gameImage_url] = [juego.id, juego.name, juego.cover.image_id, `https://images.igdb.com/igdb/image/upload/t_720p/${juego.cover.image_id}.png`];

               //    console.log({ nombre, image_url });
               gamesGallery = "";
               gamesGallery += `<div class="galeria_item">`;

               if (gameImage_id != "") {
                  gamesGallery += ` <label class="option_item" onClick="checkGaleriaItem(${gameId})">
                                       <input type="checkbox" class="checkbox">
                                       <div id="${gameId}" class="option_inner twitter" style="background-image: url('${gameImage_url}');">
                                          <div class="tickmark"></div>
                                       </div>
                                    </label>`;
               } else {
                  gamesGallery += ` <label class="option_item" onClick="checkGaleriaItem(${gameId})">
                                       <input type="checkbox" class="checkbox">
                                       <div id="${gameId}" class="option_inner twitter" style="background-image: url('https://st3.depositphotos.com/23594922/31822/v/600/depositphotos_318221368-stock-illustration-missing-picture-page-for-website.jpg');">
                                          <div class="tickmark"></div>
                                       </div>
                                    </label>`;
               }

               // if (gameImage_id != "") {
               //    gamesGallery += ` <img class='galeria_imagen' src="${gameImage_url}" alt="${gameName}" title="${gameName}">
               //                      <div class='galeria_overlay'>
               //                         <p class="galeria__title">${gameName}</p>
               //                         <div class="galeria__description">
               //                            <a href="#" data-bs-toggle="modal" data-bs-whatever="${gameId}" data-bs-target="#modalBusquedaVideojuegos"><i class="fas fa-info-circle btnInfo"></i></a>
               //                            <a href="#" onClick="confToastrSuccess(event)"><i class="fas fa-plus-circle btnAgregar"></i></a>
               //                         </div>
               //                      </div>`;
               // } else {
               //    gamesGallery += ` <img class='galeria_imagen' src='https://st3.depositphotos.com/23594922/31822/v/600/depositphotos_318221368-stock-illustration-missing-picture-page-for-website.jpg' alt="${gameName}" title="${gameName}">
               //                      <div class='galeria_overlay'>
               //                         <p class="galeria__title">${gameName}</p>
               //                         <div class="galeria__description">
               //                            <a href="#" data-bs-toggle="modal" data-bs-whatever="${gameId}" data-bs-target="#modalBusquedaVideojuegos"><i class="fas fa-info-circle btnInfo"></i></a>
               //                            <a href="#" onClick="confToastrSuccess(event)"><i class="fas fa-plus-circle btnAgregar"></i></a>
               //                         </div>
               //                      </div>`;
               // }

               gamesGallery += `</div>`;

               gG += gamesGallery;
            });

            checkGallery.style.display = "block";
            galleryContainer.innerHTML += gG;
         } else {
            console.log("Ha ocurrido un error");
         }
      } catch (error) {
         console.log(error);
      }
   };

   /*--=============================================
    =            Eventos            =
    ===============================================*/
   btnBusqueda.addEventListener("click", async (e) => {
      e.preventDefault();
      getGames();
   });
})();
